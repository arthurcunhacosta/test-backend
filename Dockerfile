FROM maven:3.6.3-jdk-11-slim as packager
ADD . /unico
WORKDIR /unico
ADD ./docker-entrypoint.sh /
# Run Maven build
RUN mvn clean install
COPY ./ .

RUN mvn package -Dmaven.test.skip=true && \
    mv ./target/*.jar /run/app.jar

FROM openjdk:11-slim

COPY --from=packager /run/app.jar /var/unico/app.jar
COPY --from=packager /docker-entrypoint.sh /docker-entrypoint.sh

RUN chmod +x /docker-entrypoint.sh
EXPOSE 8080
ENTRYPOINT [ "bash", "/docker-entrypoint.sh" ]