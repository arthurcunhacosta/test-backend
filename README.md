# Unico.io - Backend Test

### Tecnologias Utilizadas

* Java 11
* Kotlin 1.5
* Spring boot
* Spring Data
* Spring Batch
* PostgreSQL
* Swagger
* Liquibase
* Mockk
* Docker

#### Iniciando o projeto

    docker-compose up

A aplicação está dockerizada. Basta usar o comando acima para subir os containers necessários.

### Documentação
A documentação foi feita com swagger.
Após inciar o projeto, acessar o link para saber como rodar, os exemplos de requisições e possíveis respostas:

* http://localhost:8080/swagger-ui.html

### Carga inicial

Ao subir a aplicação, uma job do spring batch povoa os dados do CSV
