package br.com.unico.testbackend.service

import br.com.unico.testbackend.request.MarketRequest
import br.com.unico.testbackend.response.MarketResponse

interface MarketService {
    fun findByDistrito(distrito: String): List<MarketResponse?>
    fun findByNomefeira(nomefeira: String): List<MarketResponse?>
    fun createMarket(request: MarketRequest): MarketResponse
    fun updateMarket(request: MarketRequest): MarketResponse
    fun deleteMarket(registro: String)
}