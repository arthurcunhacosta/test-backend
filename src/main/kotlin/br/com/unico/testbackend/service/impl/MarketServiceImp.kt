package br.com.unico.testbackend.service.impl

import br.com.unico.testbackend.handler.MarketErrorCode.MARKET_DUPLICATED
import br.com.unico.testbackend.handler.MarketErrorCode.MARKET_NOT_FOUND
import br.com.unico.testbackend.handler.exception.MarketDuplicatedException
import br.com.unico.testbackend.handler.exception.MarketNotFoundException
import br.com.unico.testbackend.model.Market
import br.com.unico.testbackend.repository.MarketRepository
import br.com.unico.testbackend.request.MarketRequest
import br.com.unico.testbackend.response.MarketResponse
import br.com.unico.testbackend.service.MarketService
import org.springframework.stereotype.Service

@Service
class MarketServiceImp(private val repository: MarketRepository) : MarketService {

    override fun findByDistrito(distrito: String): List<MarketResponse?> {
        return repository.findByDistrito(distrito).map(Market::toMarketResponse)
    }

    override fun findByNomefeira(nomefeira: String): List<MarketResponse?> {
        return repository.findByNomefeira(nomefeira).map(Market::toMarketResponse)
    }

    override fun createMarket(request: MarketRequest): MarketResponse {
        validateDuplicateMarket(request.registro)
        return repository.save(request.toModel()).toMarketResponse()
    }

    override fun updateMarket(request: MarketRequest): MarketResponse {
        val market = validateExistMarket(request.registro)
        val marketToUpdate = market.copy(
            long = request.long,
            lat = request.lat,
            setcens = request.setcens,
            areap = request.areap,
            coddist = request.coddist,
            distrito = request.distrito,
            codsubpref = request.codsubpref,
            subprefe = request.subprefe,
            regiao5 = request.regiao5,
            regiao8 = request.regiao8,
            nomefeira = request.nomefeira,
            registro = request.registro,
            logradouro = request.logradouro,
            numero = request.numero,
            bairro = request.bairro,
            referencia = request.referencia
        )
        return repository.save(marketToUpdate).toMarketResponse()
    }

    override fun deleteMarket(registro: String) = repository.delete(validateExistMarket(registro))

    fun validateExistMarket(registro: String) =
        repository.findByRegistro(registro) ?: throw MarketNotFoundException(MARKET_NOT_FOUND)

    fun validateDuplicateMarket(registro: String) {
        val market = repository.findByRegistro(registro)
        if (market != null) {
            throw MarketDuplicatedException(MARKET_DUPLICATED, registro)
        }
    }

}