package br.com.unico.testbackend

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TestBackendApplication

fun main(args: Array<String>) {
	runApplication<TestBackendApplication>(*args)
}
