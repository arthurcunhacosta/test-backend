package br.com.unico.testbackend.model

import br.com.unico.testbackend.response.MarketResponse
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
data class Market(
    @Id @GeneratedValue(strategy = GenerationType.AUTO) var id: Long?,
    var long: Double,
    var lat: Double,
    var setcens: String,
    var areap: String,
    var coddist: Int,
    var distrito: String,
    var codsubpref: Int,
    var subprefe: String,
    var regiao5: String,
    var regiao8: String,
    var nomefeira: String,
    var registro: String,
    var logradouro: String,
    var numero: String?,
    var bairro: String,
    var referencia: String?
) {
    fun toMarketResponse() = MarketResponse(
        this.id,
        this.long,
        this.lat,
        this.setcens,
        this.areap,
        this.coddist,
        this.distrito,
        this.codsubpref,
        this.subprefe,
        this.regiao5,
        this.regiao8,
        this.nomefeira,
        this.registro,
        this.logradouro,
        this.numero,
        this.bairro,
        this.referencia
    )
}