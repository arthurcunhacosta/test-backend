package br.com.unico.testbackend.repository

import br.com.unico.testbackend.model.Market
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface MarketRepository : JpaRepository<Market, Long> {
    fun findByDistrito(distrito: String): List<Market>
    fun findByNomefeira(nomefeira: String): List<Market>
    fun findByRegistro(registro: String): Market?
}