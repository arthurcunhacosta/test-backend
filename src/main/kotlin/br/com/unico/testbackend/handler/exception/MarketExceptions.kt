package br.com.unico.testbackend.handler.exception

import br.com.unico.testbackend.handler.MarketErrorCode

class MarketNotFoundException(private val marketErrorCode: MarketErrorCode) : RuntimeException() {

    val errorCodeMessage: String
        get() = marketErrorCode.error
}

class MarketDuplicatedException(private val marketErrorCode: MarketErrorCode, private val registro: String) :
    RuntimeException() {

    val errorCodeMessage: String
        get() = marketErrorCode.error

    val registroDuplicated: String
        get() = registro
}