package br.com.unico.testbackend.handler

import br.com.unico.testbackend.handler.exception.MarketDuplicatedException
import br.com.unico.testbackend.handler.exception.MarketNotFoundException
import br.com.unico.testbackend.response.MarketErrorResponse
import org.slf4j.LoggerFactory
import org.springframework.context.MessageSource
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import java.util.*

@ControllerAdvice
class MarketExceptionHandler(val messageSource: MessageSource) {

    @ExceptionHandler(MarketNotFoundException::class)
    fun handleMarketNotFound(exception: MarketNotFoundException, locale: Locale?): ResponseEntity<MarketErrorResponse> {
        log.error(exception.message, exception)
        val errorMessage = messageSource.getMessage(exception.errorCodeMessage, arrayOf(), locale!!)
        return ResponseEntity(MarketErrorResponse(errorMessage), HttpStatus.NOT_FOUND)
    }

    @ExceptionHandler(MarketDuplicatedException::class)
    fun handleMarketNotFound(
        exception: MarketDuplicatedException,
        locale: Locale?
    ): ResponseEntity<MarketErrorResponse> {
        log.error(exception.message, exception)
        val errorMessage =
            messageSource.getMessage(exception.errorCodeMessage, arrayOf(exception.registroDuplicated), locale!!)
        return ResponseEntity(MarketErrorResponse(errorMessage), HttpStatus.BAD_REQUEST)
    }

    companion object {
        private val log = LoggerFactory.getLogger(MarketExceptionHandler::class.java)
    }
}