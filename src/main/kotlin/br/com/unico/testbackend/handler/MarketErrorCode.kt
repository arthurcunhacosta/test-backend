package br.com.unico.testbackend.handler

enum class MarketErrorCode(val error: String) {
    MARKET_NOT_FOUND("market.not_found"),
    MARKET_DUPLICATED("market.duplicated")
}