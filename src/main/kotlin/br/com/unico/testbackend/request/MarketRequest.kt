package br.com.unico.testbackend.request

import br.com.unico.testbackend.model.Market

data class MarketRequest(
    val long: Double,
    val lat: Double,
    val setcens: String,
    val areap: String,
    val coddist: Int,
    val distrito: String,
    val codsubpref: Int,
    val subprefe: String,
    val regiao5: String,
    val regiao8: String,
    val nomefeira: String,
    val registro: String,
    val logradouro: String,
    val numero: String?,
    val bairro: String,
    val referencia: String?
) {
    fun toModel() = Market(
        null,
        this.long,
        this.lat,
        this.setcens,
        this.areap,
        this.coddist,
        this.distrito,
        this.codsubpref,
        this.subprefe,
        this.regiao5,
        this.regiao8,
        this.nomefeira,
        this.registro,
        this.logradouro,
        this.numero,
        this.bairro,
        this.referencia
    )
}