package br.com.unico.testbackend.controller

import br.com.unico.testbackend.request.MarketRequest
import br.com.unico.testbackend.response.MarketResponse
import br.com.unico.testbackend.service.MarketService
import io.swagger.annotations.ApiOperation
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/markets")
class MarketController(val service: MarketService) {

    @ApiOperation("Create new Market")
    @PostMapping
    fun createMarket(@Valid @RequestBody request: MarketRequest): MarketResponse {
        return service.createMarket(request)
    }

    @ApiOperation("Create an existing Market")
    @PutMapping()
    fun updateMarket(@RequestBody request: MarketRequest): MarketResponse {
        return service.updateMarket(request)
    }

    @ApiOperation("Find Market by distrito")
    @GetMapping("/find-by/distrito/{distrito}")
    fun findByDistrito(@PathVariable distrito: String): List<MarketResponse?> {
        return service.findByDistrito(distrito)
    }

    @ApiOperation("Find Market by nome feira")
    @GetMapping("/find-by/nomefeira/{nomefeira}")
    fun findByNomefeira(@PathVariable nomefeira: String): List<MarketResponse?> {
        return service.findByNomefeira(nomefeira)
    }

    @ApiOperation("Delete an existing Market")
    @DeleteMapping("/{registro}")
    fun deleteMarket(@PathVariable registro: String) {
        return service.deleteMarket(registro)
    }
}