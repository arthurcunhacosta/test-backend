package br.com.unico.testbackend.config

import br.com.unico.testbackend.model.Market
import org.springframework.batch.item.ItemProcessor

class DBLogProcessor : ItemProcessor<Market, Market> {
    override fun process(market: Market): Market {
        println("Inserting market: $market")
        return market
    }
}