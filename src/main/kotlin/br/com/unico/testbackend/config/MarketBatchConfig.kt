package br.com.unico.testbackend.config

import br.com.unico.testbackend.model.Market
import org.springframework.batch.core.Job
import org.springframework.batch.core.Step
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory
import org.springframework.batch.item.ItemProcessor
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider
import org.springframework.batch.item.database.JdbcBatchItemWriter
import org.springframework.batch.item.file.FlatFileItemReader
import org.springframework.batch.item.file.LineMapper
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper
import org.springframework.batch.item.file.mapping.DefaultLineMapper
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.Resource
import javax.sql.DataSource

@Configuration
@EnableBatchProcessing
class MarketBatchConfig(
    private val jobBuilderFactory: JobBuilderFactory,
    private val stepBuilderFactory: StepBuilderFactory,
    private val dataSource: DataSource,
    @Value("classPath:/input/DEINFO_AB_FEIRASLIVRES_2014.csv") private val inputResource: Resource
) {

    @Bean
    fun readCSVFileJob(): Job {
        return jobBuilderFactory
            .get("readCSVFileJob") //                .incrementer(new RunIdIncrementer())
            .start(step())
            .build()
    }

    @Bean
    fun step(): Step {
        return stepBuilderFactory
            .get("step")
            .chunk<Market, Market>(5)
            .reader(reader())
            .processor(processor())
            .writer(writer())
            .build()
    }

    @Bean
    fun processor(): ItemProcessor<Market, Market> {
        return DBLogProcessor()
    }

    @Bean
    fun reader(): FlatFileItemReader<Market> {
        val itemReader = FlatFileItemReader<Market>()
        itemReader.setLineMapper(lineMapper())
        itemReader.setLinesToSkip(1)
        itemReader.setResource(inputResource)
        return itemReader
    }

    @Bean
    fun lineMapper(): LineMapper<Market> {
        val lineMapper = DefaultLineMapper<Market>()
        val lineTokenizer = DelimitedLineTokenizer()
        lineTokenizer.setNames(
            "id",
            "long",
            "lat",
            "setcens",
            "areap",
            "coddist",
            "distrito",
            "codsubpref",
            "subprefe",
            "regiao5",
            "regiao8",
            "nomefeira",
            "registro",
            "logradouro",
            "numero",
            "bairro",
            "referencia"
        )
        lineTokenizer.setIncludedFields(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17)
        val fieldSetMapper = BeanWrapperFieldSetMapper<Market>()
        fieldSetMapper.setTargetType(Market::class.java)
        lineMapper.setLineTokenizer(lineTokenizer)
        lineMapper.setFieldSetMapper(fieldSetMapper)
        return lineMapper
    }

    @Bean
    fun writer(): JdbcBatchItemWriter<Market> {
        val itemWriter = JdbcBatchItemWriter<Market>()
        itemWriter.setDataSource(dataSource)
        itemWriter.setSql(
            "INSERT INTO MARKET VALUES (" +
                    ":id, :long, :lat, :setcens, :areap, :coddist, :distrito, " +
                    " :codsubpref, :subprefe, :regiao5, :regiao8, :nomefeira," +
                    " :registro,  :logradouro,:numero, :bairro, :referencia)"
        )
        itemWriter.setItemSqlParameterSourceProvider(BeanPropertyItemSqlParameterSourceProvider())
        return itemWriter
    }
}