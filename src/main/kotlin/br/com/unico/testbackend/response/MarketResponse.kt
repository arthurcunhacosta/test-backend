package br.com.unico.testbackend.response

data class MarketResponse(
    val id: Long?,
    val long: Double,
    val lat: Double,
    val setcens: String,
    val areap: String,
    val coddist: Int,
    val distrito: String,
    val codsubpref: Int,
    val subprefe: String,
    val regiao5: String,
    val regiao8: String,
    val nomefeira: String,
    val registro: String,
    val logradouro: String,
    val numero: String?,
    val bairro: String,
    val referencia: String?
) {
}