package br.com.unico.testbackend.response;

data class MarketErrorResponse(var message: String)