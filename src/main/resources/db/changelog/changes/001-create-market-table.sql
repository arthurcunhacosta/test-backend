--liquibase formatted sql

--changeset liquibase:1
CREATE TABLE MARKET(
    ID BIGINT PRIMARY KEY NOT NULL,
    LONG DOUBLE PRECISION NOT NULL,
    LAT DOUBLE PRECISION NOT NULL,
    SETCENS VARCHAR(50) NOT NULL,
    AREAP VARCHAR(50) NOT NULL,
    CODDIST SMALLINT NOT NULL,
    DISTRITO VARCHAR(200) NOT NULL,
    CODSUBPREF SMALLINT NOT NULL,
    SUBPREFE VARCHAR(200) NOT NULL,
    REGIAO5 VARCHAR(50) NOT NULL,
    REGIAO8 VARCHAR(50) NOT NULL,
    NOMEFEIRA VARCHAR(200) NOT NULL,
    REGISTRO VARCHAR(20) NOT NULL,
    LOGRADOURO VARCHAR(255) NOT NULL,
    NUMERO VARCHAR(20),
    BAIRRO VARCHAR(100) NOT NULL,
    REFERENCIA VARCHAR(255)
)

--changeset liquibase:2
CREATE SEQUENCE hibernate_sequence START 881 ;