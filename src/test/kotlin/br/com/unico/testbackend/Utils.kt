package br.com.unico.testbackend

import br.com.unico.testbackend.model.Market
import br.com.unico.testbackend.request.MarketRequest

fun createMarketRequest(): MarketRequest {
    return MarketRequest(
        -4.6610332E7,
        -2.3536131E7,
        "355030810000027",
        "3550308005005",
        25,
        "Santa Monica",
        34,
        "Uber",
        "Leste",
        "Leste 1",
        "Ortizio",
        "ABC",
        "Ortizio Borges",
        "S/N",
        "Santa Monica",
        "Avenida"
    )
}

fun createMarket(): Market {
    return Market(
        1,
        -4.6610332E7,
        -2.3536131E7,
        "355030810000027",
        "3550308005005",
        25,
        "Santa Monica",
        34,
        "Uber",
        "Leste",
        "Leste 1",
        "Ortizio",
        "ABC",
        "Ortizio Borges",
        "S/N",
        "Santa Monica",
        "Avenida"
    )
}