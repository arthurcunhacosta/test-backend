package br.com.unico.testbackend.service.impl

import br.com.unico.testbackend.createMarket
import br.com.unico.testbackend.createMarketRequest
import br.com.unico.testbackend.handler.MarketErrorCode
import br.com.unico.testbackend.handler.exception.MarketDuplicatedException
import br.com.unico.testbackend.handler.exception.MarketNotFoundException
import br.com.unico.testbackend.repository.MarketRepository
import io.mockk.Runs
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

internal class MarketServiceImpTest {

    private val marketRepository = mockk<MarketRepository>()
    private val marketService = MarketServiceImp(marketRepository)

    @Test
    fun `Test create new Market`() {
        val marketRequest = createMarketRequest()
        val market = createMarket()

        every { marketRepository.save(any()) } returns market
        every { marketRepository.findByRegistro(any()) } returns null

        val createdMarket = marketService.createMarket(marketRequest)
        assertNotNull(createdMarket)
        assertEquals(1, createdMarket.id)
        assertEquals("355030810000027", createdMarket.setcens)
    }

    @Test
    fun `Test create new Market with duplicated registro`() {
        val marketRequest = createMarketRequest()
        val market = createMarket()

        every { marketRepository.findByRegistro(any()) } returns market

        val exception = assertThrows<MarketDuplicatedException> { marketService.createMarket(marketRequest) }
        assertNotNull(exception)
        assertEquals(MarketErrorCode.MARKET_DUPLICATED.error, exception.errorCodeMessage)
    }

    @Test
    fun `Test update an existing Market`() {
        val marketRequest = createMarketRequest()
        val market = createMarket()

        every { marketRepository.findByRegistro(any()) } returns market
        every { marketRepository.save(any()) } returns market

        val updateMarket = marketService.updateMarket(marketRequest)
        assertNotNull(updateMarket)
        assertEquals(1, updateMarket.id)
    }

    @Test
    fun `Test update a nonexisting Market`() {
        val marketRequest = createMarketRequest()

        every { marketRepository.findByRegistro(any()) } returns null

        val exception = assertThrows<MarketNotFoundException> { marketService.updateMarket(marketRequest) }
        assertNotNull(exception)
        assertEquals(MarketErrorCode.MARKET_NOT_FOUND.error, exception.errorCodeMessage)
    }

    @Test
    fun `Test find Market by Distrito`() {
        val market = createMarket()

        every { marketRepository.findByDistrito(any()) } returns arrayListOf(market)

        val marketFoundedList = marketService.findByDistrito("Santa Monica")
        assertNotNull(marketFoundedList)
        assertEquals(1, marketFoundedList.size)
        assertEquals("Santa Monica", marketFoundedList[0]?.distrito)
    }

    @Test
    fun `Test find Market by Nome feira`() {
        val market = createMarket()

        every { marketRepository.findByNomefeira(any()) } returns arrayListOf(market)

        val marketFoundedList = marketService.findByNomefeira("Ortizio")
        assertNotNull(marketFoundedList)
        assertEquals(1, marketFoundedList.size)
        assertEquals("Ortizio", marketFoundedList[0]?.nomefeira)
    }

    @Test
    fun `Test delete an existing market`() {
        every { marketRepository.findByRegistro(any()) } returns createMarket()
        every { marketRepository.delete(any()) } just Runs
        marketService.deleteMarket("Ortizio")
    }

    @Test
    fun `Test delete a nonexisting market`() {
        every { marketRepository.findByRegistro(any()) } returns null
        every { marketRepository.delete(any()) } just Runs

        val exception = assertThrows<MarketNotFoundException> { marketService.deleteMarket("Ortizio") }
        assertNotNull(exception)
        assertEquals(MarketErrorCode.MARKET_NOT_FOUND.error, exception.errorCodeMessage)
    }
}